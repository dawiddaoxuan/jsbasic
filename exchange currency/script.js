let button = document.querySelector(".btn");
let selectField = document.querySelector('.fieldcurrency');
let plnfield = document.querySelector(".plnfield");
let sum = 0;
let string = "";

 function SelectedField(){

  let currency = 0;

  switch (selectField.value) {  

    case "EUR" : 
    currency = 0.23;
    sum = currency * plnfield.value;
    string = " Euro's";
    return sum;
    break;

    case "USD" : 
    currency = 0.25;
    sum = currency * plnfield.value;
    string = " Dollars";
    return sum;
    break;

    case "GBP" : 
    currency = 0.2;
    sum = currency * plnfield.value
    string = " Pounds";
    return sum;
    break;

    case "YEN" : 
    currency = 0.5622;
    sum = currency * plnfield.value
    string = " Chinese Yuans";
    return sum;
    break;

    default : alert("Choose any currency!");

  };

}
    function Calculate () {

      let pln = document.querySelector(".pln");
    
      if (isNaN(plnfield.value) || plnfield.value < 0) {
    
        pln.innerHTML = "Someone's on the overdraft hmm ?"
      }
      else {
        pln.innerHTML = plnfield.value + 'PLN <br> is <br>' + SelectedField(sum).toFixed(2) + string;
    
      }
    };

   button.addEventListener("click", function(){
      Calculate();
    }); 
    
    selectField.addEventListener("change", function(){
      SelectedField();
    });